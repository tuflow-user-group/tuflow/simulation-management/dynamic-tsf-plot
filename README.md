This script uses the TSF dashboard within the TUFLOW Dashboard Script that can be found here: https://gitlab.com/tuflow-user-group/tuflow/qa-dashboards/tuflow_dashboard_example.  However, in this instance the dashboard will dynamically update during the course of the simulation.  The user is required to specify the location of the TSF on line 66.  Once the script is run, go to http://127.0.0.1:8050/ and you will see a dashboard of the TSF, this will dynamically update during the course of the simulation. 

![TSF Dashboard Image](https://gitlab.com/tuflow-user-group/tuflow/qa-dashboards/tuflow_dashboard_example/-/wikis/uploads/c921daef0081c2562bb7c148cdcee0a3/In_Progress_Sim.PNG)*In Simulation TSF Dashboard Summary*

For best usage with TUFLOW Classic, combine with a regular TSF output interval using the 'TSF Output Interval' Command within TUFLOW.
